cc.Class({
    extends: cc.Component,

    properties: {
        //星星预制资源
        starPrefab:{
            default:null,
            type:cc.Prefab
        },
        //星星消失时间范围
        maxStarDuration:0,
        minStarDuration:0,
        //地面节点，用于确定星星生成的高度
        groud:{
            default:null,
            type:cc.Node
        },
        scoreDisplay:{
            default:null,
            type:cc.Label
        },

        //player节点，获取主角弹跳的高度
        player:{
            default:null,
            type:cc.Node
        },
        scoreAudio:{
            default:null,
            url:cc.AudioClip
        },
        runing:true,
        playButton:{
            default:null,
            type:cc.Node
        },
        coinxiaoshiAnimation:{
            default:null,
            type:cc.Node
        },
    },

    playScoreSound:function(){
        cc.audioEngine.playEffect(this.scoreAudio,false);
    },

    spawnNewStar:function(){
        if(!this.runing){
            return;
        }
        var newStar=cc.instantiate(this.starPrefab);
        this.currentStar=newStar;
        this.node.addChild(newStar);
        newStar.setPosition(this.getNewStarPosition());
        newStar.getComponent('Star').game=this;
        this.starDuration=this.minStarDuration+cc.random0To1()*(this.maxStarDuration-this.minStarDuration);
        this.timer=0;
    },
    getNewStarPosition:function(){
        var randX=0;
        var randY=this.randY+cc.random0To1()*this.player.getComponent('Player').jumpHeight+50;
        var maxRandx=this.node.width/2;
        randX=cc.randomMinus1To1()*maxRandx;
        return cc.p(randX,randY);
    },

    gainScore:function(){
        this.playScoreSound();
        this.score+=1;
        this.scoreDisplay.string = 'Score: ' + this.score.toString();
    },
    gainScoreAnimation:function(starPosition){
        var coinxiaoshi=this.coinxiaoshiAnimation.getComponent("coinxiaoshi");
        //播放动画
        coinxiaoshi.playAnimation(starPosition);
    },

    // use this for initialization
    onLoad: function () {
        this.init();
        
    },
    init:function(){
        //获取地平面的y轴坐标
        this.groudY=this.groud.y+this.groud.height/2;

        //初始化定时器
        this.timer=0;
        this.starDuration=0;

        this.score=0;
        this.runing=false;

        this.playButton.node.x=0;
        this.playButton.node.y=0;

        this.player.getComponent("Player").game=this;
    },
    
    onStartGame:function(){
        this.init();
         //设置游戏运行
         this.runing=true;
         
        //设置开始按钮的位置移出屏幕之外
        this.playButton.node.x=4000;

        //生成新星星
        this.spawnNewStar();

        //启动怪物动作
        this.player.getComponent('Player').startPlayer();
    },

    // called every frame, uncomment this function to activate update callback
    update: function (dt) {
        if(this.runing){
            if(this.timer>this.starDuration){
                this.gameOver();
                return;
            }
            this.timer+=dt;     
        }
    },

    gameOver:function(){
        //设置游戏停止
        this.runing=false;

        //清除定时器
        this.clearTimer();

        //销毁星星
        if(this.currentStar!=null){
            this.currentStar.destroy();
        }

        //显示开始按钮
        this.playButton.node.x=0;
        this.playButton.node.y=0;

        //停止怪物所有动作
        this.player.getComponent('Player').stopAllActions();
        //cc.director.loadScene('game'); 
    },

    //清除定时器
    clearTimer:function(){
        this.timer=0;
    },
});
