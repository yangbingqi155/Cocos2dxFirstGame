cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
    },

    // use this for initialization
    onLoad: function () {
        this.node.setPosition(2000,0);
        //var animState= anim.play('coinxiaosh');
        // var animState=anim.getAnimationState('coinxiaosh');
        //anim.play();
    },

    playAnimation:function(starPosition){
        this.node.setPosition(starPosition);
        var anim=this.getComponent(cc.Animation);
        anim.play();
    },


    onAnimationCompleted:function(){
        this.node.setPosition(2000,0);
        //cc.log("Animation completed.");
    },
    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
