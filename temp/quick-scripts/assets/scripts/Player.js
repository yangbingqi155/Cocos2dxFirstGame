(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/Player.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'fa35fb+//VLY53Ji+EOgUB4', 'Player', __filename);
// scripts/Player.js

"use strict";

cc.Class({
    extends: cc.Component,

    //属性
    properties: {
        //跳起高度
        jumpHeight: 0,
        //持续时间
        jumpDuration: 0,
        //最大移动速度
        maxMoveSpeed: 0,
        //平均移动速度
        accel: 0,
        //跳跃音效
        jumpAudio: {
            default: null,
            url: cc.AudioClip
        },
        moveKeyboardListener: null,
        isMoving: false
    },

    //播放跳跃音乐
    playJumpSound: function playJumpSound() {
        cc.audioEngine.playEffect(this.jumpAudio, false);
    },

    setJumpAction: function setJumpAction() {
        //向上跳
        var jumpUp = cc.moveBy(this.jumpDuration, cc.p(0, this.jumpHeight)).easing(cc.easeCubicActionOut());
        //下落
        var jumpDown = cc.moveBy(this.jumpDuration, cc.p(0, -this.jumpHeight)).easing(cc.easeCubicActionIn());

        var callback = cc.callFunc(this.playJumpSound, this);

        return cc.repeatForever(cc.sequence(jumpUp, jumpDown, callback));
    },

    setInputControl: function setInputControl() {
        var self = this;
        this.moveKeyboardListener = cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function onKeyPressed(keyCode, event) {
                switch (keyCode) {
                    case cc.KEY.a:
                        self.accLeft = true;
                        self.accRight = false;
                        break;
                    case cc.KEY.d:
                        self.accLeft = false;
                        self.accRight = true;
                        break;
                }
            },
            onKeyReleased: function onKeyReleased(keyCode, event) {
                switch (keyCode) {
                    case cc.KEY.a:
                        self.accLeft = false;
                        break;
                    case cc.KEY.d:
                        self.accRight = false;
                        break;
                }
            }
        }, self.node);
    },

    // use this for initialization
    onLoad: function onLoad() {
        this.init();
    },

    init: function init() {
        this.accLeft = false;
        this.accRight = false;
        this.xSpeed = 0;
        this.isMoving = false;
        this.node.x = 0;
        this.node.y = -121;
    },

    startPlayer: function startPlayer() {
        this.init();
        this.isMoving = true;

        this.jumpAction = this.setJumpAction();
        this.node.runAction(this.jumpAction);

        this.setInputControl();
    },

    update: function update(dt) {
        this.xMoving(dt);
    },

    stopAllActions: function stopAllActions() {
        //停止水平移动
        this.stopMove();
        //停止上下跳跃
        if (this.jumpAction != null) {
            this.node.stopAction(this.jumpAction);
            this.jumpAction = null;
        }
    },

    xMoving: function xMoving(dt) {
        //开关控制是否水平移动
        if (this.isMoving) {
            //计算加速度
            if (this.accLeft) {
                this.xSpeed -= this.accel * dt;
            } else if (this.accRight) {
                this.xSpeed += this.accel * dt;
            }

            //加速度不能超过最大速度
            if (Math.abs(this.xSpeed) > this.maxMoveSpeed) {
                this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed);
            }

            //当怪物触碰边界时弹回
            if (Math.abs(this.node.x) >= this.game.node.width / 2) {
                this.xSpeed = -this.xSpeed;
            }

            //更新主角X轴的位置
            this.node.x += this.xSpeed * dt;
        }
    },

    stopMove: function stopMove() {

        //水平移动开关关闭
        this.isMoving = false;

        //禁用左右移动键盘输入
        this.disableInputControl();

        //设置不往水平左右移动
        this.accLeft = false;
        this.accRight = false;

        //水平移动速度设置为0
        this.xSpeed = 0;
    },

    //禁用左右移动键盘输入
    disableInputControl: function disableInputControl() {
        cc.eventManager.removeListener(this.moveKeyboardListener);
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Player.js.map
        